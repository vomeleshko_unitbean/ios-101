//
//  ArticleRoutes.swift
//  iOS-101
//
//  Created by Vladislav Meleshko on 15.02.2018.
//  Copyright © 2018 Vladislav Meleshko. All rights reserved.
//

import Foundation
import UIKit

class ArticleRoutes {
    static func showArticle(article: ArticleModel, fromVc: UIViewController){
        let articleVC = UIStoryboard(name: "Article", bundle: nil).instantiateViewController(withIdentifier: ArticleTableViewController.nibName) as! ArticleTableViewController
        
        articleVC.dataProvider.article = article
        
        fromVc.navigationController?.pushViewController(articleVC, animated: true)
    }
}
