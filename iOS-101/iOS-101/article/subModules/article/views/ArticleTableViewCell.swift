//
//  ArticleTableViewCell.swift
//  iOS-101
//
//  Created by Vladislav Meleshko on 15.02.2018.
//  Copyright © 2018 Vladislav Meleshko. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    static let nibName = "ArticleTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var bodyLabel: UILabel!
    
    func customize(article: ArticleModel){
        titleLabel.text = article.title
        bodyLabel.text = article.body
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
