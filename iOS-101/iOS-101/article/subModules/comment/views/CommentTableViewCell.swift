//
//  CommentTableViewCell.swift
//  iOS-101
//
//  Created by Vladislav Meleshko on 15.02.2018.
//  Copyright © 2018 Vladislav Meleshko. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    static let nibName = "CommentTableViewCell"

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    func customize(comment: CommentModel) {
        nameLabel.text = comment.name
        commentLabel.text = comment.body
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
