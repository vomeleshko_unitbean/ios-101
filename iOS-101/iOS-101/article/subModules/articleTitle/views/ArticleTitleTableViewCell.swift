//
//  ArticleTitleTableViewCell.swift
//  iOS-101
//
//  Created by Vladislav Meleshko on 15.02.2018.
//  Copyright © 2018 Vladislav Meleshko. All rights reserved.
//

import UIKit

class ArticleTitleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    static let nibName = "ArticleTitleTableViewCell"
    
    func customize(article: ArticleModel) {
        titleLabel.text = article.title
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
