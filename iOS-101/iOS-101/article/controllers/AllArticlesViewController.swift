//
//  AllArticlesViewController.swift
//  iOS-101
//
//  Created by Vladislav Meleshko on 15.02.2018.
//  Copyright © 2018 Vladislav Meleshko. All rights reserved.
//

import UIKit

class AllArticlesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AllArticlesDataProviderDelegate {
    
    
    // MARK: PARAMS
    
    @IBOutlet weak var tableView: UITableView!
    var dataProvider: AllArticlesDataProvider = AllArticlesDataProvider()
    
    // MARK: CONTROLLER
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataProvider.delegate = self
        
        customizeTableView()
        
        dataProvider.loadArticles()
        
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TABLE VIEW
    
    func customizeTableView(){
        tableView.register(
            UINib(nibName: ArticleTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleTableViewCell.nibName)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProvider.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticleTableViewCell.nibName) as! ArticleTableViewCell
        cell.customize(article: dataProvider.articles[indexPath.row])        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        return 
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // performSegue(withIdentifier: "toArticle", sender: nil)
        ArticleRoutes.showArticle(article: dataProvider.articles[indexPath.row], fromVc: self)
        
    }
    
    // MARK: - AllArticlesDataProviderDelegate
    
    func articlesDataLoaded() {
        tableView.reloadData()
    }
    
    func articlesDataHasError(error: String) {
        // TODO: реализовать обработку ошибки
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
