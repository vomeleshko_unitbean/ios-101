//
//  CommentModel.swift
//  iOS-101
//
//  Created by Vladislav Meleshko on 19.02.2018.
//  Copyright © 2018 Vladislav Meleshko. All rights reserved.
//

import ObjectMapper

class CommentModel: NSObject, Mappable {
    var postId: Int?
    var id: Int?
    var name: String?
    var email: String?
    var body: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        postId <- map["postId"]
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
        body <- map["body"]
    }
}
