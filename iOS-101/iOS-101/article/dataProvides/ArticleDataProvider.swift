//
//  ArticleDataProvider.swift
//  iOS-101
//
//  Created by Vladislav Meleshko on 19.02.2018.
//  Copyright © 2018 Vladislav Meleshko. All rights reserved.
//

import Foundation

protocol ArticleDataProviderDelegete: class {
    func articleDataDidLoad()
    func articleDataHasError(error: String)
}

class ArticleDataProvider: NSObject {
    
    var article: ArticleModel = ArticleModel()
    var comments: [CommentModel] = []
    
    weak var delegate: ArticleDataProviderDelegete?
    
    func refresh() {
        comments = []
        loadData()
    }
    
    func loadData() {
        if article.id != nil {
            SharedApiService.sharedInstance.commentService.comments(postId: article.id!, complection: { (commentsResponse, errors) in
                if let error = errors {
                    self.delegate?.articleDataHasError(error: error)
                    return
                } else if let commentsArray:[CommentModel] = commentsResponse {
                    self.comments.append(contentsOf: commentsArray)
                    self.delegate?.articleDataDidLoad()
                }
            })

        }
    }
}
