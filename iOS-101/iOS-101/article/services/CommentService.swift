//
//  CommentService.swift
//  iOS-101
//
//  Created by Vladislav Meleshko on 19.02.2018.
//  Copyright © 2018 Vladislav Meleshko. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyJSON

typealias CommentsComplection = (_ comments: [CommentModel]?, _ error: String?) -> Void


class CommentService: TypicodeApiService {
    
    func comments(postId: Int, complection: @escaping CommentsComplection) {
        let url = host + "/comments"
        
        var params:[String: AnyObject] = [:]
        params["postId"] = postId as AnyObject
        
        self.sendRequestWithJSONResponse(
            requestType: HTTPMethod.get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                if error != nil {
                    complection(nil, error!.localizedDescription)
                    return
                } else if let comments = responseData!.arrayObject {
                    let commentModel = Mapper<CommentModel>().mapArray(JSONObject: comments)
                    complection(commentModel, nil)
                    return
                }
                
                complection(nil, "Ошибка загрузки комментариев")
                
        }
    }
}

