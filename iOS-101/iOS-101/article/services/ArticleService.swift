//
//  ArticleService.swift
//  iOS-101
//
//  Created by Vladislav Meleshko on 16.02.2018.
//  Copyright © 2018 Vladislav Meleshko. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyJSON

typealias ArticlesPostCompletion = (_ articles: [ArticleModel]?, _ error: String? ) -> Void

class ArticleService: TypicodeApiService {
    
    func post(limit: Int, skip: Int, completion: @escaping ArticlesPostCompletion){
        let url = host + "/posts"
        
        var params:[String: AnyObject] = [:]
        params["limit"] = limit as AnyObject
        params["skip"] = skip as AnyObject
        
        self.sendRequestWithJSONResponse(
            requestType: HTTPMethod.get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                
                if error != nil {
                    completion(nil, error!.localizedDescription)
                    return
                } else if let articles = responseData!.arrayObject {
                    let articleModels = Mapper<ArticleModel>().mapArray(JSONObject: articles)
                    
                    completion(articleModels, nil)
                    return
                }
                
                completion(nil, "Ошибка загрузки каталога")
                
        }
        
        
        
    }
}
